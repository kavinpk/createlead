package week1.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {
public void login()	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("AMAZON");
		driver.findElementById("createLeadForm_firstName").sendKeys("Kavin");
		driver.findElementById("createLeadForm_lastName").sendKeys("PK");
		
		
		WebElement eleSource =driver.findElementById("createLeadForm_dataSourceId");
		Select sc = new Select(eleSource);
		sc.selectByVisibleText("Conference");
		
		WebElement Source =driver.findElementById("createLeadForm_marketingCampaignId");
		Select se = new Select(Source);
		se.selectByValue("9000");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("siva");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("pk");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("online app");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Shopping");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Purchase");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("2345.45");
		
		
		// select by visible id for industry
		
		
		WebElement dropdown = driver.findElementById("createLeadForm_industryEnumId");
		Select sw = new Select(dropdown);
		List<WebElement> list = sw.getOptions();
		int count = list.size();
		System.out.println(count);
		sw.selectByIndex(count-1);
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("4500");
		driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Partnership");
		driver.findElementById("createLeadForm_sicCode").sendKeys("4320");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("AMO");
	    driver.findElementById("createLeadForm_description").sendKeys("The Amazon.in marketplace is operated by Amazon Seller Services Private Ltd, an affiliate of Amazon.com, Inc. (NASDAQ: AMZN). Amazon.in seeks to build the most customer-centric online destination for customers to find and discover virtually anything they want to buy online by giving them more of what they want � vast selection, low prices, fast and reliable delivery, and a trusted and convenient experience; and provide sellers with a world-class e-commerce marketplace. For more information, visit www.amazon.in/aboutus. Join Amazon Newsroom: Twitter | Facebook");
	    driver.findElementById("createLeadForm_importantNote").sendKeys("Kuldeep, who is part of Amazon Web Services, is one of the 40-odd Indians and the only Amazonian from our country to have ever successfully completed IRONMAN, one of the world�s toughest triathlons.");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("+1");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("456 67 48 345");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("+1 456 45 67 890");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Movit Ramwani ");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("movitr@amazon.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("https://www.amazon.in/");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Urvashi Athavale");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Urvashi");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("4th street luna towers");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("2nd floor");
		driver.findElementById("createLeadForm_generalCity").sendKeys("New Jersy");
		WebElement Findlast = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select selist = new Select(Findlast);
		List<WebElement> options = selist.getOptions();
		int size = options.size();
		System.out.println(size);
		selist.selectByIndex(count-1);
		
		
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("34567");
		driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("United States");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("3456");
		driver.findElementByName("submitButton").click();
	}
}